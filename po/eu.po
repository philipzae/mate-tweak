# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Asier Iturralde Sarasola <asier.iturralde@gmail.com>, 2015
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009
msgid ""
msgstr ""
"Project-Id-Version: MATE Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-06-12 16:49+0100\n"
"PO-Revision-Date: 2016-05-20 17:25+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Basque (http://www.transifex.com/mate/MATE/language/eu/)\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../mate-tweak:949
msgid "Marco (No compositor)"
msgstr ""

#: ../mate-tweak:951
msgid "Marco (Software compositor)"
msgstr ""

#: ../mate-tweak:953
msgid "Marco (No effects xcompmgr compositor)"
msgstr ""

#: ../mate-tweak:955
msgid "Marco (Compton GPU compositor)"
msgstr ""

#: ../mate-tweak:957
msgid "Metacity (No compositor)"
msgstr ""

#: ../mate-tweak:959
msgid "Metacity (Software compositor)"
msgstr ""

#: ../mate-tweak:961
msgid "Metacity (No effects xcompmgr compositor)"
msgstr ""

#: ../mate-tweak:963
msgid "Metacity (Compton GPU compositor)"
msgstr ""

#: ../mate-tweak:965
msgid "Mutter (Elegant GPU accelerated desktop effects)"
msgstr ""

#: ../mate-tweak:967
msgid "Compiz (Advanced GPU accelerated desktop effects)"
msgstr "Compiz (GPU bidez azeleratutako mahaigaineko efektu aurreratuak)"

#: ../mate-tweak:970
msgid ""
"You are currently using an unknown and unsupported window manager. Thus we "
"cannot guarantee that changes made here will be effective."
msgstr ""

#: ../mate-tweak:988
msgid "Cupertino"
msgstr ""

#: ../mate-tweak:991
msgid "Fedora"
msgstr ""

#: ../mate-tweak:994
msgid "GNOME2"
msgstr ""

#: ../mate-tweak:997
msgid "Linux Mint"
msgstr "Linux Mint"

#: ../mate-tweak:1000
msgid "Mageia"
msgstr ""

#: ../mate-tweak:1005
msgid "Mutiny"
msgstr ""

#: ../mate-tweak:1008
msgid "Netbook"
msgstr ""

#: ../mate-tweak:1011
msgid "openSUSE"
msgstr "openSUSE"

#: ../mate-tweak:1014
msgid "Redmond"
msgstr "Redmond"

#: ../mate-tweak:1017
msgid "Ubuntu MATE"
msgstr "Ubuntu MATE"

#: ../mate-tweak:1020
msgid "Wimpy"
msgstr ""

#: ../mate-tweak:1074
msgid "Desktop"
msgstr "Mahaigaina"

#: ../mate-tweak:1075
msgid "Interface"
msgstr "Interfazea"

#: ../mate-tweak:1076
msgid "Panel"
msgstr ""

#: ../mate-tweak:1077
msgid "Windows"
msgstr "Leihoak"

#: ../mate-tweak:1120
msgid "MATE Tweak"
msgstr ""

#: ../mate-tweak:1123
msgid "Desktop icons"
msgstr "Mahaigaineko ikonoak"

#: ../mate-tweak:1124
msgid "Performance"
msgstr "Errendimendua"

#: ../mate-tweak:1125
msgid "Window Behaviour"
msgstr ""

#: ../mate-tweak:1126
msgid "Appearance"
msgstr "Itxura"

#: ../mate-tweak:1127
msgid "Panels"
msgstr ""

#: ../mate-tweak:1128
msgid "Panel Features"
msgstr ""

#: ../mate-tweak:1129
msgid "Panel Menu Features"
msgstr ""

#: ../mate-tweak:1130
msgid "Icons"
msgstr "Ikonoak"

#: ../mate-tweak:1131
msgid "Context menus"
msgstr "Testuinguru menuak"

#: ../mate-tweak:1132
msgid "Toolbars"
msgstr "Tresna-barrak"

#: ../mate-tweak:1133
msgid "Window manager"
msgstr ""

#: ../mate-tweak:1135
msgid "Select the Desktop Icons you want enabled:"
msgstr ""

#: ../mate-tweak:1136
msgid "Show Desktop Icons"
msgstr ""

#: ../mate-tweak:1137
msgid "Computer"
msgstr "Ordenagailua"

#: ../mate-tweak:1138
msgid "Home"
msgstr "Karpeta nagusia"

#: ../mate-tweak:1139
msgid "Network"
msgstr "Sarea"

#: ../mate-tweak:1140
msgid "Trash"
msgstr "Zakarrontzia"

#: ../mate-tweak:1141
msgid "Mounted Volumes"
msgstr "Muntatutako bolumenak"

#: ../mate-tweak:1143
msgid "Enable animations"
msgstr ""

#: ../mate-tweak:1144
msgid "Do not show window content when moving windows"
msgstr ""

#: ../mate-tweak:1145
msgid "Window manager performance tuning."
msgstr ""

#: ../mate-tweak:1147
msgid "Enable window snapping"
msgstr ""

#: ../mate-tweak:1148
msgid "Undecorate maximized windows"
msgstr ""

#: ../mate-tweak:1149
msgid "Do not auto-maximize new windows"
msgstr ""

#: ../mate-tweak:1151
msgid "Window control placement."
msgstr ""

#: ../mate-tweak:1153
msgid "Save Panel Layout"
msgstr ""

#: ../mate-tweak:1154
msgid "Enable indicators"
msgstr ""

#: ../mate-tweak:1155
msgid "Enable advanced menu"
msgstr ""

#: ../mate-tweak:1156
msgid "Enable keyboard LED"
msgstr ""

#: ../mate-tweak:1157
msgid "Enable launcher"
msgstr ""

#: ../mate-tweak:1158
msgid "Enable pull-down terminal"
msgstr ""

#: ../mate-tweak:1160
msgid "Show Applications"
msgstr ""

#: ../mate-tweak:1161
msgid "Show Places"
msgstr ""

#: ../mate-tweak:1162
msgid "Show System"
msgstr ""

#: ../mate-tweak:1164
msgid "Show icons on menus"
msgstr "Erakutsi ikonoak menuetan"

#: ../mate-tweak:1165
msgid "Show icons on buttons"
msgstr "Erakutsi ikonoak botoietan"

#: ../mate-tweak:1166
msgid "Show Input Methods menu in context menus"
msgstr "Erakutsi Sarrera Metodoak menua testuinguru menuetan"

#: ../mate-tweak:1167
msgid "Show Unicode Control Character menu in context menus"
msgstr "Erakutsi Unicode Kontrol Karaktereen menua testuinguru menuetan"

#: ../mate-tweak:1169
msgid "Style:"
msgstr ""

#: ../mate-tweak:1170
msgid "Icon size:"
msgstr "Ikonoen tamaina:"

#: ../mate-tweak:1189
msgid "Small"
msgstr "Txikia"

#: ../mate-tweak:1190
msgid "Large"
msgstr "Handia"

#: ../mate-tweak:1196
msgid "Traditional (Right)"
msgstr "Tradizionala (eskuinean)"

#: ../mate-tweak:1197
msgid "Contemporary (Left)"
msgstr "Gaur egungoa (ezkerrean)"

#: ../mate-tweak:1203
msgid "The new window manager will be activated upon selection."
msgstr "Hautatutakoan aktibatuko da leiho kudeatzaile berria."

#: ../mate-tweak:1204
msgid "Select a window manager."
msgstr ""

#: ../mate-tweak:1210
msgid "Select a panel layout to change the user interface."
msgstr ""

#: ../mate-tweak:1211
msgid ""
"The new panel layout will be activated on selection and destroy any "
"customisations you might have made."
msgstr ""
"Panel diseinu berria hautatutakoan aktibatuko da eta aurretik egin dituzun "
"pertsonalizazio guztiak desegingo ditu."

#: ../mate-tweak:1218
msgid "Default"
msgstr ""

#: ../mate-tweak:1219
msgid "16px"
msgstr ""

#: ../mate-tweak:1220
msgid "22px"
msgstr ""

#: ../mate-tweak:1221
msgid "24px"
msgstr ""

#: ../mate-tweak:1222
msgid "32px"
msgstr ""

#: ../mate-tweak:1223
msgid "48px"
msgstr ""

#: ../mate-tweak:1227
msgid "Set the panel icon size."
msgstr ""

#: ../mate-tweak:1228
msgid "Select the icon size for panel icons."
msgstr ""

#: ../mate-tweak:1232
msgid "Set the icon size of menu items used in the panel."
msgstr ""

#: ../mate-tweak:1233
msgid "Select the icon size for menu items in the panel."
msgstr ""

#: ../mate-tweak:1254
msgid "Text below items"
msgstr "Testua elementuen azpian"

#: ../mate-tweak:1255
msgid "Text beside items"
msgstr "Testua elementuen ondoan"

#: ../mate-tweak:1256
msgid "Icons only"
msgstr "Ikonoak soilik"

#: ../mate-tweak:1257
msgid "Text only"
msgstr "Testua soilik"
